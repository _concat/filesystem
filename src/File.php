<?php

namespace Concat\Filesystem;

use Concat\Filesystem\Instance\File as Instance;

class File
{
    public static function getInstance($path)
    {
        return new Instance($path);
    }

    public static function create($path)
    {
        return self::getInstance($path)->create();
    }

    public static function copy($origin, $target)
    {
        return self::getInstance($origin)->copyTo($target);
    }

    public static function move($origin, $target)
    {
        return self::getInstance($origin)->moveTo($target);
    }

    public static function createParent($path)
    {
        return self::getInstance($path)->createParent();
    }

    public static function exists($path)
    {
        return self::getInstance($path)->exists();
    }

    public static function size($path)
    {
        return self::getInstance($path)->getSize();
    }

    public static function putContents($path, $data)
    {
        return self::getInstance($path)->putContents($data);
    }

    public static function getContents($path)
    {
        return self::getInstance($path)->getContents();
    }

    public static function delete($path)
    {
        return self::getInstance($path)->delete();
    }
}
