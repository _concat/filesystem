<?php

namespace Concat\Filesystem\Instance;

// use \Concat\Filesystem\Facade\Path;
// use \Concat\Filesystem\Facade\File;
// use \Concat\Filesystem\Facade\Directory as Dir;

class Directory extends Path
{
    public function create($recursive = true, $mode = 0777)
    {
        if (!$this->exists()) {
            mkdir($this->getPathname(), $mode, $recursive);
        }

        return $this;
    }

    // lists all folders and files
    public function iterator($recursive = true)
    {
        $options =
            \FilesystemIterator::KEY_AS_PATHNAME |
            \FilesystemIterator::CURRENT_AS_FILEINFO |
            \FilesystemIterator::SKIP_DOTS;

        if ($recursive) {
            $d = new \RecursiveDirectoryIterator($this->getPathname(), $options);
            $i = new \RecursiveIteratorIterator($d, \RecursiveIteratorIterator::CHILD_FIRST);
        } else {
            $d = new \FilesystemIterator($this->getPathname(), $options);
            $i = new \IteratorIterator($d);
        }

        return $i;
    }

    public function index($recursive = true, $options = Path::F | Path::D)
    {
        $list = [];

        $abs = $options & Path::A;

        $offset = strlen($this->getPathname()) + 1;

        foreach ($this->iterator($recursive) as $path => $file) {
            if ($abs) {
                $path = $file->getRealPath();
            } else {
                $path = substr($path, $offset);
            }

            if ($file->isFile()) {
                if ($options & Path::F) {
                    $list[] = $path;
                }
            } else {
                if ($options & Path::D) {
                    $list[] = $path;
                }
            }
        }

        return $list;
    }

    public function clean($recursive = true)
    {
        foreach ($this->iterator($recursive) as $path) {
            if ($path->isFile()) {
                $file = new File($path);
                $file->delete();
            } elseif ($recursive) {
                $directory = new Directory($path);
                $directory->clean();
            }
        }

        return $this;
    }

    public function isEmpty()
    {
        $handle = opendir($this->getPathname());

        while (($entry = readdir($handle)) !== false) {
            if ($entry !== "." && $entry !== "..") {
                return false;
            }
        }

        return true;
    }

    public function clear()
    {
        foreach ($this->iterator() as $path) {
            $path = $path->isFile()

                //
                ? new File($path)

                //
                : new Directory($path);

            //
            $path->delete();
        }

        // do not delete itself
        return $this;
    }

    public function delete()
    {
        //$path = Path::get($path);

        if ($this->exists()) {
            $this->clear();

            rmdir($this->getPathname());
        }

        return $this;
    }

    public function getSize()
    {
        // initial size of the directory itself
        $size = filesize($this->getPathname());

        foreach ($this->iterator() as $path) {
            $size += filesize($path);
        }

        return $size;
    }

    public function copyTo($target)
    {
        $offset = strlen($this->getPathname()) + 1;

        foreach ($this->iterator() as $path => $file) {
            $relative = substr($path, $offset);

            $destination = new Path($target, $relative);

            if ($file->isFile()) {
                $origin = new File($path);
                $origin->copyTo($destination);
            } else {
                $directory = new Directory($destination);
                $directory->create();
            }
        }

        return $this;
    }

    public function renameTo($name)
    {
        $name = (new Directory($name))->getBasename();

        $target = new Directory($this->getParent(), $name);

        $origin = $this->getPathname();

        parent::__construct($target);

        return rename($origin, $target);
    }

    public function moveTo($target)
    {
        $destination = new Directory($target);
        $destination->createParent();

        $origin = $this->getPathname();

        parent::__construct($destination);

        return rename($origin, $destination);
    }
}
