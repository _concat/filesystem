<?php

namespace Concat\Filesystem\Instance;

class Path extends \SplFileInfo
{
    const FILE = 1;
    const F = 1;

    const DIRECTORY = 2;
    const DIR = 2;
    const D = 2;

    const ABSOLUTE = 4;
    const ABS = 4;
    const A = 4;

    private function join(array $paths)
    {
        $depth = count($paths);

        if ($depth === 0) {
            return ".";
        }

        if ($depth === 1) {
            return $paths[0];
        }

        $absolute = strval($paths[0])[0] === "/";

        $path = join("/", array_map(function ($path) {
            return trim($path, "/");
        }, $paths));

        if ($absolute) {
            $path = "/".$path;
        }

        return $path;
    }

    public function __construct(...$paths)
    {
        parent::__construct($this->join($paths));
    }

    public function setPath(...$paths)
    {
        parent::__construct($this->join($paths));
    }

    public function setExtension($extension)
    {
        if ($this->getExtension() !== $extension) {
            parent::__construct($this->getPathname().".".$extension);
        }
    }

    public function createParent()
    {
        $parent = new Directory($this->getParent());

        return $parent->create();
    }

    public function getParent()
    {
        return new Directory($this->getPath());
    }

    public function exists()
    {
        return file_exists($this->getPathname());
    }
}
