<?php

namespace Concat\Filesystem\Instance;

class File extends Path
{
    public function create()
    {
        if (!$this->exists()) {
            $this->createParent();

            return touch($this->getPathname());
        }
    }

    public function copyTo($target)
    {
        $target = new File($target);
        $target->createParent();

        return copy($this->getPathname(), $target);
    }

    public function moveTo($target)
    {
        $target = new File($target);
        $target->createParent();

        $origin = $this->getPathname();
        parent::__construct($target);

        return rename($origin, $target);
    }

    public function getContents()
    {
        if ($this->exists()) {
            return file_get_contents($this);
        }
    }

    public function putContents($data)
    {
        file_put_contents($this, $data);
    }

    public function getSize()
    {
        return filesize($this->getPathname());
    }

    public function delete()
    {
        unlink($this->getPathname());
    }
}
