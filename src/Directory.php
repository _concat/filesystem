<?php

namespace Concat\Filesystem;

use Concat\Filesystem\Instance\Directory as Instance;
use Concat\Filesystem\Instance\Path;

class Directory
{
    public static function getInstance($path)
    {
        return new Instance($path);
    }

    public static function create($path, $recursive = true, $mode = 0777)
    {
        return self::getInstance($path)->create($recursive, $mode);
    }

    // lists all folders and files
    public static function iterator($path, $recursive = true)
    {
        return self::getInstance($path)->iterator($recursive);
    }

    public static function createParent($path)
    {
        return self::getInstance($path)->createParent();
    }

    public static function index($path, $recursive = true, $options = Path::F | Path::D)
    {
        return self::getInstance($path)->index($recursive, $options);
    }

    public static function clean($path, $recursive = true)
    {
        return self::getInstance($path)->clean($recursive);
    }

    public static function isEmpty($path)
    {
        return self::getInstance($path)->isEmpty();
    }

    public static function clear($path)
    {
        return self::getInstance($path)->clear();
    }

    public static function rename($path, $name)
    {
        return self::getInstance($path)->renameTo($name);
    }

    public static function exists($path)
    {
        return self::getInstance($path)->exists();
    }

    public static function delete($path)
    {
        return self::getInstance($path)->delete();
    }

    public static function size($path)
    {
        return self::getInstance($path)->getSize();
    }

    public static function copy($origin, $target)
    {
        return self::getInstance($origin)->copyTo($target);
    }

    public static function move($origin, $target)
    {
        return self::getInstance($origin)->moveTo($target);
    }
}
