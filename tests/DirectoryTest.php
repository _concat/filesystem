<?php

namespace Concat\Filesystem\Tests;

use Concat\Filesystem\Directory;
use Concat\Filesystem\Instance\Path;
use Concat\Filesystem\Instance\Directory as Instance;

class DirectoryTest extends \PHPUnit_Framework_TestCase
{
    use FilesystemTest;

    private function _index($recursive, $flags, ...$paths)
    {
        foreach ($paths as $path) {
            $index = Directory::index($path, $recursive, $flags);

            $r = $recursive ? "_r" : "";

            $expected = [];
            if ($flags & Path::D) {
                $expected = array_merge($expected, $this->paths["dirs$r"]);
            }

            if ($flags & Path::F) {
                $expected = array_merge($expected, $this->paths["files$r"]);
            }

            if ($flags & Path::A) {
                // absolute
                $expected = array_map(function ($x) {
                    return PATH."/".$x;
                }, $expected);
            }

            sort($index);
            sort($expected);

            $this->assertEquals($expected, $index);
        }
    }

    private function index($recursive, $flags)
    {
        $this->_index($recursive, $flags,
            PATH."/",
            PATH,
            "tests/paths/base",
            "tests/../tests/paths/base",
            "tests/../tests/paths/base/",
            "tests/../tests/paths/base/."
        );
    }

    public function test_size()
    {
        // this is hard to test because it seems to be different across platforms?
        $this->assertTrue(Directory::size(PATH) > 0);
    }

    public function test_clear()
    {
        Directory::clear(PATH);

        $this->assertEmpty(Directory::index(PATH));
    }

    public function test_clean()
    {
        Directory::clean(PATH, false);

        $expected = $this->paths['dirs'];
        $actual = Directory::index(PATH, false);

        sort($actual);
        sort($expected);

        $this->assertEquals($expected, $actual);
    }

    public function test_clean_recursive()
    {
        Directory::clean(PATH);

        $expected = $this->paths['dirs_r'];
        $actual = Directory::index(PATH);

        sort($actual);
        sort($expected);

        $this->assertEquals($expected, $actual);
    }

    public function test_rename_static()
    {
        $name = "test";

        $old = new Path(PATH, "parent", "child");
        $new = new Path(PATH, "parent", $name);

        $this->assertFalse(Directory::exists($new));

        Directory::create($old);
        Directory::rename($old, $name);

        $this->assertTrue(Directory::exists($new));
        $this->assertFalse(Directory::exists($old));
    }

    public function test_iterator_static()
    {
        $iterator = Directory::iterator(PATH);

        $offset = strlen(PATH) + 1;

        $paths = [];
        foreach ($iterator as $path) {
            $paths[] = substr($path->getPathname(), $offset);
        }

        $expected = array_merge($this->paths["files_r"], $this->paths["dirs_r"]);

        sort($expected);
        sort($paths);

        $this->assertEquals($expected, $paths);
    }

    public function test_rename_instance()
    {
        $name = "test";

        $old = new Path(PATH, "parent", "child");
        $new = new Path(PATH, "parent", $name);

        $this->assertFalse(Directory::exists($new));

        $instance = new Instance($old);
        $instance->create();
        $instance->renameTo($name);

        $this->assertTrue(Directory::exists($new));
        $this->assertFalse(Directory::exists($old));

        $this->assertEquals($new->getPathname(), $instance->getPathname());
    }

    public function test_create_parent()
    {
        $path = new Path(ROOT, "parent", "child");

        $this->assertFalse(Directory::exists(dirname($path)));

        Directory::createParent($path);

        $this->assertTrue(Directory::exists(dirname($path)));
        $this->assertFalse(Directory::exists($path));
    }

    public function test_copy()
    {
        $from = PATH;

        $to = new Path(ROOT, "copied");

        $expected = Directory::index($from);
        Directory::copy($from, $to);
        $actual = Directory::index($to);

        sort($expected);
        sort($actual);

        $this->assertEquals($expected, $actual);
    }

    public function test_move()
    {
        $from = PATH;
        $to = new Path(ROOT, "moved");

        $expected = Directory::index($from);
        Directory::move($from, $to);
        $actual = Directory::index($to);

        sort($expected);
        sort($actual);

        $this->assertEquals($expected, $actual);
    }

    public function test_exists()
    {
        $this->assertTrue(Directory::exists(PATH));
        Directory::delete(PATH);
        $this->assertFalse(Directory::exists(PATH));
    }

    public function test_delete()
    {
        Directory::delete(PATH);

        $this->assertFalse(Directory::exists(PATH));
    }

    public function test_create()
    {
        $p = __DIR__."/paths/x";

        Directory::create($p);

        $this->assertTrue(file_exists($p) && is_dir($p) && count(scandir($p)) === 2);
    }

    public function test_is_empty()
    {
        $p =  __DIR__."/paths/x";

        Directory::create($p);

        $this->assertTrue(Directory::isEmpty($p));
        $this->assertTrue(Directory::isEmpty("$p/."));
        $this->assertFalse(Directory::isEmpty("$p/.."));
    }

    ///

    public function test_relative_index_dir_recursive()
    {
        $this->index(true, Path::D);
    }

    public function test_relative_index_dir()
    {
        $this->index(false, Path::D);
    }

    //

    public function test_relative_index_files()
    {
        $this->index(false, Path::F);
    }

    public function test_relative_index_files_recursive()
    {
        $this->index(true, Path::F);
    }

        ///


    public function test__index_dir_recursive()
    {
        $this->index(true, Path::D | Path::A);
    }

    public function test_index_dir()
    {
        $this->index(false, Path::D | Path::A);
    }

    ///


    public function test_index()
    {
        $this->index(false, Path::D | Path::F | Path::A);
    }

    public function test_index_recursive()
    {
        $this->index(true, Path::D | Path::F | Path::A);
    }

    //


    public function test_index_files()
    {
        $this->index(false, Path::F | Path::A);
    }

    public function test_index_files_recursive()
    {
        $this->index(true, Path::F | Path::A);
    }

    //

    public function test_relative_index()
    {
        $this->index(false, Path::D | Path::F);
    }

    public function test_relative_index_recursive()
    {
        $this->index(true, Path::D | Path::F);
    }
}
