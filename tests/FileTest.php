<?php

namespace Concat\Filesystem\Tests;

use Concat\Filesystem\File;
use Concat\Filesystem\Directory;
use Concat\Filesystem\Instance\Path;
use Concat\Filesystem\Instance\File as Instance;

class FileTest extends \PHPUnit_Framework_TestCase
{
    use FilesystemTest;

    public function test_put_contents()
    {
        $file = new Instance(ROOT, "parent", "datafile");
        $data = "data";

        $file->createParent();
        $file->putContents($data);

        $this->assertEquals(file_get_contents($file), $data);
    }

    public function test_get_contents()
    {
        $file = new Instance(ROOT, "parent", "datafile");
        $data = "data";

        $this->assertNull($file->getContents());

        $file->create();

        file_put_contents($file, $data);
        $this->assertEquals($file->getContents(), $data);
    }

    public function test_file_create()
    {
        $path = new Path(ROOT, "parent", "newfile");

        $this->assertFalse(File::exists($path));

        File::create($path);

        $this->assertTrue(File::exists($path));

        File::create($path);

        $this->assertTrue(File::exists($path));
    }

    public function test_size()
    {
        $path = new Path(PATH, 1);
        $this->assertEquals(File::size($path), 32);
    }

    public function test_move()
    {
        $src = new Instance(PATH, 1);
        $dst = new Instance(ROOT, "moved", 1);

        File::move($src, $dst);

        $this->assertTrue(File::exists($dst));
        $this->assertFalse(File::exists($src));
    }

    public function test_move_instance()
    {
        $src = new Instance(PATH, 1);
        $dst = new Instance(ROOT, "moved", 1);

        $initial = $src->getPathname();
        $expected = $dst->getPathname();

        $src->moveTo($dst);

        $this->assertEquals($expected, $src->getPathname());

        $this->assertTrue(File::exists($dst));
        $this->assertFalse(File::exists($initial));
    }

    public function test_copy()
    {
        $src = new Instance(PATH, 1);
        $dst = new Instance(ROOT, "copied", 1);

        File::copy($src, $dst);

        $this->assertTrue(File::exists($dst));
        $this->assertTrue(File::exists($src));
    }

    public function test_create_parent()
    {
        $path = new Path(ROOT, "parent", 1);

        $this->assertFalse(Directory::exists(dirname($path)));

        File::createParent($path);

        $this->assertTrue(Directory::exists(dirname($path)));

        $this->assertFalse(File::exists($path));
    }

    public function test_delete()
    {
        $src = new Instance(PATH, 1);

        $this->assertTrue(File::exists($src));
        File::delete($src);
        $this->assertFalse(File::exists($src));
    }
}
