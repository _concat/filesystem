<?php

namespace Concat\Filesystem\Tests;

define("ROOT", __DIR__."/paths");
define('PATH', __DIR__."/paths/base");

use Concat\Filesystem\Directory;

trait FilesystemTest
{
    private $paths = [

        "dirs_r" => [
            "a",
            "b",
            "c",
            "c/d",
            "c/e",
            "c/f",
            "c/f/g",
            "c/f/h",
            "c/f/i",
        ],

        "dirs" => [
            "a", "b", "c",
        ],

        "files" => [
            "1", "2",
        ],

        "files_r" => [
            "1",
            "2",
            "c/1",
            "c/2",
            "c/f/1",
            "c/f/2",
            "c/f/3",
        ],
    ];

    private function createFiles()
    {
        foreach ($this->paths['files_r'] as $path) {
            $data = md5(time());
            file_put_contents(PATH."/".$path, $data);
        }
    }

    private function createDirs()
    {
        foreach ($this->paths['dirs_r'] as $path) {
            mkdir(PATH."/".$path, 0777, true);
        }
    }

    public function setUp()
    {
        Directory::delete(ROOT);

        // create files and directories
        $this->createDirs();
        $this->createFiles();
    }

    public function tearDown()
    {
        // remove the paths directory completely
        Directory::delete(ROOT);
    }
}
