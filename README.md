[![Author](http://img.shields.io/badge/author-@rudi_theunissen-blue.svg?style=flat)](https://twitter.com/rudi_theunissen)
[![Build Status](https://img.shields.io/travis/concat/filesystem/master.svg?style=flat)](https://travis-ci.org/concat/filesystem)
[![Software License](https://img.shields.io/packagist/l/concat/filesystem.svg?style=flat)](LICENSE.md)
[![Latest Version](https://img.shields.io/github/tag/concat/filesystem.svg?style=flat&label=release)](https://github.com/concat/filesystem/tags)
[![Total Downloads](https://img.shields.io/packagist/dt/concat/filesystem.svg?style=flat)](https://packagist.org/packages/concat/filesystem)
[![Version](https://img.shields.io/packagist/v/concat/filesystem.svg?style=flat)](https://packagist.org/packages/concat/filesystem)
[![Test Coverage](https://codeclimate.com/github/concat/filesystem/badges/coverage.svg?style=flat)](https://codeclimate.com/github/concat/filesystem)
[![Code Climate](https://codeclimate.com/github/concat/filesystem/badges/gpa.svg?style=flat)](https://codeclimate.com/github/concat/filesystem)

This package is compliant with [PSR-1], [PSR-2] and [PSR-4]. If you notice compliance oversights,
please send a patch via pull request.

[PSR-1]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md
[PSR-2]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md
[PSR-4]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md

## Install

Via Composer

``` json
require: {
    "concat/filesystem": "dev-master"
}
```
